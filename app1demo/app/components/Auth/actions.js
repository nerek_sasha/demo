import {
  auth_UPDATE
} from './constants';

export const authUpdate = (payload = {}) => ({
  type: auth_UPDATE,
  payload,
});
