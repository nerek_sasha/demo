import React from 'react';
import { FormattedMessage } from 'react-intl';
import style from 'styled-components';
import { Row, Col, Form, Icon, Input, Button, Checkbox  } from 'antd';
const FormItem = Form.Item;


import { connect } from 'react-refetch'

import { bindActionCreators } from 'redux';
import { connect as connectRedux } from 'react-redux';
import * as pageActions from './actions';

import messages from './messages';

class Auth extends React.Component {

  componentWillReceiveProps = (nextProps) => {
    try {
      if (nextProps.respAuth && !nextProps.respAuth.pending && nextProps.respAuth.fulfilled) {
        if(nextProps.respAuth.value) {
          this.props.pageActions.authUpdate({ auth: nextProps.respAuth.value.data });
        }
      }
    }catch (e){ console.log(e.message); }
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const {
          userName,
          password,
        } = values;
        this.props.reqAuth({
          login: userName,
          password: password,
        });
      }
    });
  };
  render = () => {
    const { getFieldDecorator } = this.props.form;
    return (
      <div style={{ width: '100%', paddingTop: 25 }}>
        <Row>
          <Col span={9} >&nbsp;</Col>
          <Col span={6}>
            <Form onSubmit={this.handleSubmit} className="login-form">
              <FormItem>
                {getFieldDecorator('userName', {
                  rules: [{ required: true, message: 'Please input your username!' }],
                })(
                  <Input prefix={<Icon type="user" style={{ fontSize: 13 }} />} placeholder="Username" />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('password', {
                  rules: [{ required: true, message: 'Please input your Password!' }],
                })(
                  <Input prefix={<Icon type="lock" style={{ fontSize: 13 }} />} type="password" placeholder="Password" />
                )}
              </FormItem>
              <FormItem>
                {/*<div>
                  {getFieldDecorator('remember', {
                    valuePropName: 'checked',
                    initialValue: true,
                  })(
                    <Checkbox>Remember me</Checkbox>
                  )}
                </div>*/}
                <div>
                  <Button type="primary" htmlType="submit" className="login-form-button" style={{width: '100%'}}>
                    Log in
                  </Button>
                </div>
              </FormItem>
            </Form>
          </Col>
          <Col span={9} >&nbsp;</Col>
        </Row>
      </div>
    );
  }
}

const FormAuth = Form.create()(Auth);

const AuthRefetch = connect(props => ({
  reqAuth: (data) => ({
    respAuth: {
      url: `https://dev.butler-hero.org/api/internal/v1/user/auth`,
      method: 'POST',
      body: JSON.stringify(data),
      force: true,
      refreshing: true
    }
  })
}))(FormAuth);

export default connectRedux((state) => ({}), (dispatch) => ({
  pageActions: bindActionCreators(pageActions, dispatch)
}))(AuthRefetch);
