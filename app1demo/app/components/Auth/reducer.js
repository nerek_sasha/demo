import { fromJS } from 'immutable';

import {
  auth_UPDATE
} from './constants';

const initialState = fromJS({
  auth: {},
});

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case auth_UPDATE:
      return state
        .set('auth', action.payload.auth);
    default:
      return state;
  }
};

export default authReducer;
