import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Layout } from 'antd';
import styled from 'styled-components';

const FooterBlock = Layout.Footer;

import messages from './messages';

class Footer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render = () => {
    return (
      <FooterBlock style={{ textAlign: 'center' }}>
       Admin Panel @2017
      </FooterBlock>
    );
  }
}

export default Footer;
