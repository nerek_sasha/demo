import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Layout, Menu, Icon } from 'antd';
import styled from 'styled-components';

const Head = Layout.Header;

import HeaderLink from './HeaderLink';
import messages from './messages';
import LocaleToggle from 'containers/LocaleToggle';

const LocaleToggleItem = styled.div`
  float: right !important;
  display: inline-block;
`;

class Header extends React.Component { // eslint-disable-line react/prefer-stateless-function
  state = {
    current: 'mail',
  };
  handleClick = (e) => {
    this.setState({
      current: e.key,
    });
  };
  render = () => {
    return (
      <Head style={{ height: 72 }}>
        <div className="logo" />
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={['2']}
          style={{ lineHeight: '64px' }}
        >
          <Menu.Item key="dash-board">
            <HeaderLink to="/">
              <Icon type="pie-chart" /><FormattedMessage {...messages.home} />
            </HeaderLink>
          </Menu.Item>
          <Menu.Item key="battlers">
            <HeaderLink to="/battlers">
              <Icon type="user" /><FormattedMessage {...messages.battlers} />
            </HeaderLink>
          </Menu.Item>
          <Menu.Item key="clients">
            <HeaderLink to="/clients">
              <Icon type="user" /><FormattedMessage {...messages.clients} />
            </HeaderLink>
          </Menu.Item>
          <Menu.Item key="orders">
            <HeaderLink to="/orders">
              <Icon type="shopping-cart" /><FormattedMessage {...messages.orders} />
            </HeaderLink>
          </Menu.Item>
          <LocaleToggleItem >
            <LocaleToggle/>
          </LocaleToggleItem>
        </Menu>
      </Head>
    );
  }
}

export default Header;
