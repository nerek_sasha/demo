/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  home: {
    id: 'boilerplate.components.Header.home',
    defaultMessage: 'Dashboard',
  },
  battlers: {
    id: 'boilerplate.components.Header.battlers',
    defaultMessage: 'Battlers',
  },
  clients: {
    id: 'boilerplate.components.Header.clients',
    defaultMessage: 'Clients',
  },
  orders: {
    id: 'boilerplate.components.Header.orders',
    defaultMessage: 'Orders',
  },
});
