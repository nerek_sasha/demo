import React from 'react';
import Helmet from 'react-helmet';
import styled from 'styled-components';
import { Layout } from 'antd';
import { LocaleProvider } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';

const { Content } = Layout;

import { bindActionCreators } from 'redux';
import { connect as connectRedux } from 'react-redux';
import * as pageActions from './actions';

import Header from 'components/Header';
import Footer from 'components/Footer';
import Auth from 'components/Auth';
import withProgressBar from 'components/ProgressBar';

import 'antd/dist/antd.css';
import 'font-awesome/css/font-awesome.css';

const AppWrapper = styled.div`
  margin: 0 auto;
  display: flex;
  min-height: 100%;
  flex-direction: column;
`;

class App extends React.Component {

  static propTypes = {
    children: React.PropTypes.node,
    authData: React.PropTypes.any.isRequired,
  };
  state = {
    token: false,
  };

  componentWillMount = () => {
    if(this.props.authData['auth-token']){
      this.setState({
        token: true
      });
    }
  };

  componentWillReceiveProps = (nextProps) => {
    if(nextProps.authData['auth-token']){
      this.setState({
        token: true
      });
    }
  };

  render = () => {
    const {
      authData,
    } = this.props;
    const {
      token,
    } = this.state;
    return (
      <LocaleProvider locale={enUS}>
        <AppWrapper>
          <Helmet
            titleTemplate="CMS"
            defaultTitle="CMS"
            meta={[
              {name: '#1', content: '#2'},
            ]}
          />
          {(token) ? (
              <Layout className="layout" style={{ backgroundColor: '#fff' }}>
                <Header />
                <Content style={{ padding: '0 50px' }}>
                  {React.Children.toArray(this.props.children)}
                </Content>
                <Footer />
              </Layout>
            ) : (
              <Auth />
            )
          }
        </AppWrapper>
      </LocaleProvider>
    );
  }
}

export default connectRedux((state) => ({
  authData: state.toJS().authData ? state.toJS().authData.auth : {},
}), (dispatch) => ({}))(withProgressBar(App));
