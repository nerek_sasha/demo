import React from 'react';
import faker from 'faker';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { Table, Icon, Button, Checkbox, Input, Dropdown, Menu, Row, Col, Modal, Form } from 'antd';

import messages from './messages';

class AddBattler extends React.Component {

  static propTypes = {
    visible: React.PropTypes.bool,
    OnClose: React.PropTypes.func,
    userData: React.PropTypes.shape(),
  };

  state = {
    confirmDirty: false,
    autoCompleteResult: [],
  };

  handleOk = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        this.props.OnClose();
      }
    });
  };
  handleCancel = () => {
    this.props.OnClose();
  };
  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };
  checkPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };
  checkConfirm = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };
  render = () => {
    const {
      visible,
      userData,
    } = this.props;
    console.log(userData.name);
    const { getFieldDecorator } = this.props.form;
    return (
      <Modal title={`Edit user ID: ${userData._id}`}
             wrapClassName="vertical-center-modal"
             visible={visible}
             onOk={this.handleOk}
             onCancel={this.handleCancel}
             okText={'Create'}
             cancelText={'Cancel'}
             key={faker.random.uuid()}
      >
        <Form>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item label="">
                {getFieldDecorator('firstName', {
                  rules: [{ required: true, message: 'Please input your first name!' }],
                  initialValue: userData.name.first,
                })(
                  <Input prefix={<Icon type="user" style={{ fontSize: 13 }} /> } placeholder="First name" />
                )}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item>
                {getFieldDecorator('lastName', {
                  rules: [{ required: true, message: 'Please input your last name!' }],
                  initialValue: userData.name.last,
                })(
                  <Input prefix={<Icon type="user" style={{ fontSize: 13 }} />} placeholder="Last Name" />
                )}
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item>
                {getFieldDecorator('phone', {
                  rules: [{ required: true, message: 'Please write phone!' }],
                  initialValue: userData.phone,
                })(
                  <Input prefix={<Icon type="phone" style={{ fontSize: 13 }} />} placeholder="Phone" />
                )}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item >
                {getFieldDecorator('password', {
                  rules: [{
                    required: false, message: 'Please input your password!',
                  }, {
                    validator: this.checkConfirm,
                  }],
                })(
                  <Input type="password" prefix={<Icon type="key" style={{ fontSize: 13 }} />} placeholder="Password" />
                )}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item >
                {getFieldDecorator('confirm', {
                  rules: [{
                    required: false, message: 'Please confirm your password!',
                  }, {
                    validator: this.checkPassword,
                  }],
                })(
                  <Input type="password" onBlur={this.handleConfirmBlur} prefix={<Icon type="key" style={{ fontSize: 13 }} />} placeholder="Confirm password" />
                )}
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Modal>
    );
  }
}

export default Form.create()(AddBattler);
