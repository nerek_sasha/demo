import React from 'react';
import _ from 'lodash';
import faker from 'faker';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { Link } from 'react-router';
import { Table, Collapse, Icon, Button, Checkbox, Input, Dropdown, Menu, Row, Col, Modal, Form } from 'antd';
import { connect as connectRefetch } from 'react-refetch'

import messages from './messages';
import genv from '../../../env';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as pageActions from './actions';

const sortAlphabet = (a, b) => {
  if(a < b) return -1;
  if(a > b) return 1;
  return 0;
};

class ProfilePage extends React.Component {

  state = {
    data: {},
    changePass: false,
    userOrders: [],
    orderLoading: true,
  };

  componentWillMount = () => {
    const {
      data,
      params,
      form,
      reqOrder,
    } = this.props;
    reqOrder();
    this.setState({
      data: _.find(data, { _id: params.id }),
    });
  };

  componentWillReceiveProps = (nextProps) => {
    if(nextProps.respOrder && !nextProps.respOrder.pending && nextProps.respOrder.fulfilled){
      this.setState({
        userOrders: nextProps.respOrder.value.data,
        orderLoading: false,
      });
    }
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };
  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };
  checkPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };
  checkConfirm = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };
  render = () => {
    const {
      changePass,
      data,
      userOrders,
      orderLoading,
    } = this.state;
    const { getFieldDecorator } = this.props.form;
    return (
      <Form>
        <Row gutter={16}>
          <div style={{ marginTop: 10 }} />
          <Col span={12}>
            <Form.Item label="">
              {getFieldDecorator('firstName', {
                rules: [{ required: true, message: 'Please input your first name!' }],
                initialValue: data.name.first,
              })(
                <Input prefix={<Icon type="user" style={{ fontSize: 13 }} /> } placeholder="First name" />
              )}
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item>
              {getFieldDecorator('lastName', {
                rules: [{ required: true, message: 'Please input your last name!' }],
                initialValue: data.name.last,
              })(
                <Input prefix={<Icon type="user" style={{ fontSize: 13 }} />} placeholder="Last Name" />
              )}
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item>
              {getFieldDecorator('phone', {
                rules: [{ required: true, message: 'Please write phone!' }],
                initialValue: data.phone,
              })(
                <Input prefix={<Icon type="phone" style={{ fontSize: 13 }} />} placeholder="Phone" />
              )}
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item >
              {getFieldDecorator('password', {
                rules: [{
                  required: false, message: 'Please input your password!',
                }, {
                  validator: this.checkConfirm,
                }],
              })(
                <Input type="password" prefix={<Icon type="key" style={{ fontSize: 13 }} />} placeholder="Password" />
              )}
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item >
              {getFieldDecorator('confirm', {
                rules: [{
                  required: false, message: 'Please confirm your password!',
                }, {
                  validator: this.checkPassword,
                }],
              })(
                <Input type="password" onBlur={this.handleConfirmBlur} prefix={<Icon type="key" style={{ fontSize: 13 }} />} placeholder="Confirm password" />
              )}
            </Form.Item>
          </Col>
          <Col span={24}>
            <Collapse bordered={false}>
              <Collapse.Panel header="User orders: " key="1">
                <Table
                  columns={
                    [{
                      title: 'Name',
                      dataIndex: 'name',
                      render: (name, row) => {
                        return (<Link to={`/order/${row._id}`}>{name}</Link>);
                      },
                      sorter: (a, b) => {
                        const A = a.name.last.toLowerCase(),
                          B = b.name.last.toLowerCase();
                        return sortAlphabet(A, B);
                      },
                    }, {
                      title: 'Time create',
                      dataIndex: 'createdAt',
                      render: (createdAt) => {
                        return (<span>{createdAt}</span>);
                      },
                    }]
                  }
                  rowKey={record => record._id}
                  dataSource={userOrders}
                  loading={orderLoading}
                />
              </Collapse.Panel>
            </Collapse>
          </Col>
        </Row>
      </Form>
    );
  }
}

const FormProfilePage = Form.create()(ProfilePage);

const FormProfilePageRefetch = connectRefetch(props => ({
  reqOrder: () => {
    const {
      protocol,
      serverAddr,
      port,
    } = genv.request;
    return ({
      respOrder: {
        url: `${protocol}${serverAddr}:${port}/user/orders`,
        method: 'GET',
        force: true,
        refreshing: true
      }
    });
  }
}))(FormProfilePage);

export default connect((state) => ({
  data: state.toJS().dataBattlers ? state.toJS().dataBattlers.data : []
}), (dispatch) => ({}))(FormProfilePageRefetch);
