import {
  data_UPDATE,
  data_DELETE,
} from './constants';

export const dataUpdate = (payload = {}) => ({
  type: data_UPDATE,
  payload,
});

export const deleteUser = (payload = {}) => ({
  type: data_DELETE,
  payload,
});
