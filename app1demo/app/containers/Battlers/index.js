import React from 'react';
import _ from 'lodash';
import { FormattedMessage } from 'react-intl';
import Helmet from 'react-helmet';
import { Link } from 'react-router';
import styled from 'styled-components';
import { Table, Icon, Button, Checkbox, Input, Dropdown, Menu, Row, Col } from 'antd';
import faker from 'faker';
import FontAwesome from 'react-fontawesome';

import { connect as connectRefetch } from 'react-refetch'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as pageActions from './actions';

import messages from './messages';
import AddBattler from './AddBattler';
import Profile from './ProfileModal';
import withProgressBar from '../../components/ProgressBar';

const data = [];
for (let i = 0; i < 46; i++) {
  data.push({
    _id: faker.random.uuid(),
    phone: faker.phone.phoneNumber(),
    email: faker.internet.email(),
    addrs: [],
    location: [],
    push_sended: faker.random.boolean(),
    reviewed: faker.random.boolean(),
    phone_verified: faker.random.boolean(),
    professional: faker.random.boolean(),
    name: {
      last: faker.name.lastName(),
      first: faker.name.firstName(),
    }
  });
}

const RowColumns = styled(Row)`
  padding: 10px;
  width: 180px;
`;

const RowActions = styled(Row)`
  padding: 10px 0;
`;

const ColFloatRight = styled(Col)`
  float: right;
  text-align: right;
`;

const CheckboxGroup = Checkbox.Group;

const sortAlphabet = (a, b) => {
  if(a < b) return -1;
  if(a > b) return 1;
  return 0;
};

const defColumns = [
  { value: "name", label: "Name"},
  { value: "professional", label: "Professional"},
  { value: "phone", label: "Phone"},
  { value: "phone_verified", label: "Phone validated?"},
  { value: "email", label: "Email"},
  { value: "is_active", label: "User activ"},
  { value: "is_admin", label: "Is admin?"},
  { value: "reviewed", label: "Reviewed"},
  { value: "actions", label: "Actions"},
];

const defColumnsVisible = [
  { value: "name", label: "Name"},
  { value: "professional", label: "Professional"},
  { value: "phone", label: "Phone"},
  { value: "phone_verified", label: "Phone validated?"},
  { value: "actions", label: "Actions"},
];

class Battlers extends React.Component {

  static propTypes = {
    pageActions: React.PropTypes.shape().isRequired,
  };

  state = {
    filteredInfo: null,
    sortedInfo: null,
    filterDropdownVisible: false,
    data: [],
    dataVisible: [],
    searchText: '',
    filtered: false,
    visibleModalAddBattler: false,
    visibleModalEditBattler: false,
    editData: null,
    selectedRowKeys: [],
    visibleColumns: defColumnsVisible,
    columns: defColumns,
  };

  componentWillMount = () => {
    this.props.pageActions.dataUpdate({ data });
    // this.props.reqUsers();
  };

  componentDidMount = () => {
    console.log(this.props.data);
    this.setState({
      data: this.props.data,
      dataVisible: this.props.data,
    });
  };

  componentWillReceiveProps = (nextProps) => {
    if(nextProps.respUsers && !nextProps.respUsers.pending && nextProps.respUsers.fulfilled){
      this.props.pageActions.dataUpdate({ data: nextProps.respUsers.value });
    }
    this.setState({
      data: nextProps.data,
      dataVisible: nextProps.data,
    });
  };

  rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      this.setState({
        selectedRowKeys,
      });
    },
    getCheckboxProps: record => ({
      disabled: record.name === 'Disabled User',    // Column configuration not to be checked
    }),
  };

  handleChange = (pagination, filters, sorter) => {
    console.log('Various parameters', pagination, filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  };
  changeEditData = (data) => {
    this.setState({
      visibleModalEditBattler: true,
      editData: data,
    });
  };
  onInputChange = (e) => {
    this.setState({ searchText: e.target.value });
  };
  onSearch = () => {
    const { searchText } = this.state;
    const reg = new RegExp(searchText, 'gi');
    this.setState({
      filterDropdownVisible: false,
      filtered: !!searchText,
      dataVisible: this.props.data.map((record) => {
        const match = record.name.last.match(reg)
          || record.name.last.match(reg)
          || (`${record.name.first} ${record.name.last}`).match((reg));
        if (!match || match === null) {
          return null;
        }
        return {
          ...record,
        };
      }).filter(record => !!record),
    });
  };
  showAddBattler = () => {
    if( !this.state.visibleModalAddBattler ) {
      this.setState({
        visibleModalAddBattler: true
      });
    }
  };
  deleteBattlers = () => {
    console.log(this.state.selectedRowKeys);
  };
  render = () => {
    const {
      dataVisible,
      visibleColumns,
      columns,
      filterDropdownVisible,
      selectedRowKeys,
      visibleModalEditBattler,
      editData,
    } = this.state;

    const columnsTable = (visibleColumns.length > 0) ? _.filter([
      {
        title: 'Name',
        dataIndex: 'name',
        render: ({ first, last }, row) => {
          return (<Link to={`/battler/${row._id}`}>{first}{' '}{last}</Link>);
        },
        sorter: (a, b) => {
          const A = a.name.last.toLowerCase(),
            B = b.name.last.toLowerCase();
          return sortAlphabet(A, B);
        },
        filterDropdown: (
          <div className="custom-filter-dropdown">
            <Input
              ref={ele => this.searchInput = ele}
              placeholder="Search name"
              value={this.state.searchText}
              onChange={this.onInputChange}
              onPressEnter={this.onSearch}
            />
            <Button type="primary" onClick={this.onSearch}>Search</Button>
          </div>
        ),
        filterDropdownVisible: filterDropdownVisible,
        onFilterDropdownVisibleChange: visible => this.setState({ filterDropdownVisible: visible }, () => this.searchInput.focus()),
      },
      {
        title: 'Professional',
        dataIndex: 'professional',
        render: (val) => (<Checkbox defaultChecked={val} disabled />),
        sorter: (a, b) => (b.professional - a.professional),
      },
      {
        title: 'Phone',
        dataIndex: 'phone',
        sorter: (a, b) => {
          const A = a.phone.toLowerCase(),
            B = b.phone.toLowerCase();
          return sortAlphabet(A, B);
        },
      },
      {
        title: 'Phone validated?',
        dataIndex: 'phone_verified',
        render: (val) => (<Checkbox defaultChecked={val} disabled />),
        sorter: (a, b) => (b.phone_verified - a.phone_verified),
      },
      {
        title: 'Email',
        dataIndex: 'email',
        sorter: (a, b) => {
          const A = a.email.toLowerCase(),
            B = b.email.toLowerCase();
          return sortAlphabet(A, B);
        },
      },
      {
        title: 'User active',
        key: 'is_active',
        render: (val) => (<Checkbox defaultChecked={true} disabled />),
        sorter: (a, b) => (b.is_active - a.is_active),
      },
      {
        title: 'Is admin?',
        key: 'is_admin',
        render: (val) => (<Checkbox defaultChecked={faker.random.boolean()} disabled />),
        sorter: (a, b) => (b.is_admin - a.is_admin),
      },
      {
        title: 'Reviewed',
        dataIndex: 'reviewed',
        render: (val) => (<Checkbox defaultChecked={val} disabled />),
        sorter: (a, b) => (b.reviewed - a.reviewed),
      },
      {
        title: 'Actions',
        key: 'actions',
        render: (val) => (
          <Row>
            <Col span={8}>
              <Button
                icon={"edit"}
                onClick={() => this.changeEditData(val)}
              />
            </Col>
            <Col span={8}>
              <Button type={"danger"} icon={"delete"}
                onClick={() => {this.props.pageActions.deleteUser({ delete: val._id });}}
              />
            </Col>
          </Row>
        ),
      }
    ], (item) => (_.findIndex(visibleColumns, ({ value }) => (value === item.dataIndex || value === item.key)) !== -1 )) : [{
      title: 'ID',
      dataIndex: '_id',
      sorter: (a, b) => {
        const A = a._id.toLowerCase(),
          B = b._id.toLowerCase();
        return sortAlphabet(A, B);
      },
    }];
    return (
      <div style={{ paddingTop: 10 }}>
        <Helmet
          title="Battler Page"
          meta={[
            { name: '#1', content: '#1' },
          ]}
        />
        <div>
          <RowActions >
            <Col span={8} >
              <Button type="primary" onClick={this.showAddBattler}>
                <Icon type="user-add" /> Add Battler
              </Button>
              <div style={{ width: 10, display: 'inline-block' }} />
              <Button type="danger" ghost disabled={!(selectedRowKeys.length > 0)} onClick={ () => {this.deleteBattlers()} }>
                <Icon type="delete" /> Delete
              </Button>
              <AddBattler
                visible={this.state.visibleModalAddBattler}
                OnClose={() => {
                  this.setState({
                    visibleModalAddBattler: false
                  })
                }}
              />
              {(editData) ? (
                <Profile
                  visible={visibleModalEditBattler}
                  OnClose={() => {
                    this.setState({
                      visibleModalEditBattler: false,
                      editData: null,
                    });
                  }}
                  userData={editData}
                />
              ) : (
                <div />
              )}
            </Col>
            <ColFloatRight span={8} offset={8}>
              <Dropdown
                trigger={['click']}
                overlay={(
                  <CheckboxGroup
                    defaultValue={visibleColumns.map(item => (item.value))}
                    onChange={(values) => {
                      const tCol = _.filter(columns, (item) => (_.findIndex(values, (val) => (val === item.value)) !== -1 ));
                      this.setState({
                        visibleColumns: tCol
                      });
                    }}
                    rowKey
                  >
                    <RowColumns>
                      {columns.map(({value ,label}, index) => (<Col key={`col_for_select_${index}`} span={18} offset={3}><Checkbox value={value}>{label}</Checkbox></Col>))}
                    </RowColumns>
                  </CheckboxGroup>
                )}
                placement="bottomLeft"
              >
                <Button>Columns <Icon type="down" /></Button>
              </Dropdown>
            </ColFloatRight>
          </RowActions>
        </div>
        <Table rowSelection={this.rowSelection} rowKey={"_id"} columns={columnsTable} dataSource={dataVisible} />
      </div>
    );
  }
}

const BattlersRefetch = connectRefetch(props => ({
  reqUsers: () => ({
    respUsers: {
      url: `https://dev.butler-hero.org/api/internal/v1/user/list`,
      method: 'GET',
      headers: {
        Authorization: props.authData['auth-token'],
      },
      force: true,
      refreshing: true
    }
  })
}))(Battlers);

export default connect((state) => ({
  data: state.toJS().dataBattlers ? state.toJS().dataBattlers.data : [],
  authData: state.toJS().authData ? state.toJS().authData.auth : {},
}), (dispatch) => ({
  pageActions: bindActionCreators(pageActions, dispatch)
}))(BattlersRefetch);
