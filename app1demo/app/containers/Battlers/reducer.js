import { fromJS } from 'immutable';
import _ from 'lodash';

import {
  data_UPDATE,
  data_DELETE,
} from './constants';

const initialState = fromJS({
  data: [],
  delete: {}
});

const battlersReducer = (state = initialState, action) => {
  switch (action.type) {
    case data_UPDATE:
      return state
        .set('data', action.payload.data);
    case data_DELETE:
      let tData = state.get('data');
      tData = _.remove(tData, item => (item._id !== action.payload.delete));
      return state
        .set('data', tData);
    default:
      return state;
  }
};

export default battlersReducer;
