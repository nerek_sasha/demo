import {
  data_UPDATE
} from './constants';

export const dataUpdate = (payload = {}) => ({
  type: data_UPDATE,
  payload,
});
