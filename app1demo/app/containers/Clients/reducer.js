import { fromJS } from 'immutable';

import {
  data_UPDATE
} from './constants';

const initialState = fromJS({
  data: [],
});

const clientsReducer = (state = initialState, action) => {
  switch (action.type) {
    case data_UPDATE:
      return state
        .set('data', action.payload.data);
    default:
      return state;
  }
};

export default clientsReducer;
