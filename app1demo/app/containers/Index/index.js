import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';

export class HomePage extends React.PureComponent {
  render = () => {
    const {
      data
    } = this.props;
    return (
      <article>
        <Helmet
          title="Home Page"
          meta={[
            { name: 'meta-name', content: 'React' },
          ]}
        />
        {JSON.stringify(data)}
      </article>
    );
  }
}

HomePage.propTypes = {
  data: React.PropTypes.arrayOf(React.PropTypes.shape).isRequired,
};

export default connect((state) => ({
  data: state.toJS().dataBattlers ? state.toJS().dataBattlers.data : []
}), (dispatch) => ({}))(HomePage);
