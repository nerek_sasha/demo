import React from 'react';
import faker from 'faker';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { Table, Icon, Select, Button, Checkbox, Input, Dropdown, Menu, Row, Col, Modal, Form } from 'antd';

import messages from './messages';

class AddOrder extends React.Component {

  static propTypes = {
    visible: React.PropTypes.bool,
    OnClose: React.PropTypes.func,
  };

  state = {
    confirmDirty: false,
    autoCompleteResult: [],
  };

  handleOk = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        this.props.OnClose();
      }
    });
  };
  handleCancel = () => {
    this.props.OnClose();
  };
  render = () => {
    const {
      visible,
    } = this.props;
    const { getFieldDecorator } = this.props.form;
    return (
      <Modal title="Create a new Order"
             wrapClassName="vertical-center-modal"
             visible={visible}
             onOk={this.handleOk}
             onCancel={this.handleCancel}
             okText={'Create'}
             cancelText={'Cancel'}
             key={faker.random.uuid()}
      >
        <Form>
          <Row gutter={16} offset={4}>
            <Col span={24}>
              <Form.Item label="">
                {getFieldDecorator('name', {
                  rules: [{ required: true, message: 'Please input your order name!' }],
                })(
                  <Input prefix={<Icon type="user" style={{ fontSize: 13 }} /> } placeholder="Order name" />
                )}
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item>
                {getFieldDecorator('status', {
                  rules: [{ required: true, message: 'Please select status!' }],
                })(
                  <Select
                    showSearch
                    style={{ width: '100%' }}
                    placeholder="Select a status"
                    optionFilterProp="children"
                    onChange={(value) => {console.log(value)}}
                    filterOption={(input, option) => option.props.value.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  >
                    <Select.Option value="0">Active</Select.Option>
                    <Select.Option value="1">Canceled</Select.Option>
                    <Select.Option value="2">Completed</Select.Option>
                    <Select.Option value="1">Pending</Select.Option>
                    <Select.Option value="1">Rejected</Select.Option>
                  </Select>
                )}
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item>
                {getFieldDecorator('product', {
                  rules: [{ required: true, message: 'Please select product!' }],
                })(
                  <Select
                    showSearch
                    style={{ width: '100%' }}
                    placeholder="Product"
                    optionFilterProp="children"
                    onChange={(value) => {console.log(value)}}
                    filterOption={(input, option) => option.props.value.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  >
                    <Select.Option value="0">Prod_1</Select.Option>
                    <Select.Option value="1">Prod_2</Select.Option>
                    <Select.Option value="2">Prod_3</Select.Option>
                    <Select.Option value="1">Prod_4</Select.Option>
                    <Select.Option value="1">Prod_5</Select.Option>
                  </Select>
                )}
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item>
                {getFieldDecorator('customer', {
                  rules: [{ required: true, message: 'Please select customer!' }],
                })(
                  <Select
                    showSearch
                    style={{ width: '100%' }}
                    placeholder="Customer"
                    optionFilterProp="children"
                    onChange={(value) => {console.log(value)}}
                    filterOption={(input, option) => option.props.value.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  >
                    <Select.Option value="0">Admin 1</Select.Option>
                    <Select.Option value="1">User 2</Select.Option>
                    <Select.Option value="2">User 3</Select.Option>
                  </Select>
                )}
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item>
                {getFieldDecorator('payment_type', {
                  rules: [{ required: true, message: 'Please select payment type!' }],
                })(
                  <Select
                    showSearch
                    style={{ width: '100%' }}
                    placeholder="Payment Type"
                    optionFilterProp="children"
                    onChange={(value) => {console.log(value)}}
                    filterOption={(input, option) => option.props.value.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  >
                    <Select.Option value="0">Card</Select.Option>
                    <Select.Option value="1">Cash</Select.Option>
                    <Select.Option value="2">PayPal</Select.Option>
                  </Select>
                )}
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Modal>
    );
  }
}

export default Form.create()(AddOrder);
