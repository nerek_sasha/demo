import React from 'react';
import _ from 'lodash';
import { connect as connectRefetch, PromiseState } from 'react-refetch';
import moment from 'moment';
import faker from 'faker';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { Table, Icon, Select, Button, Checkbox, Collapse, Input, Dropdown, Menu, Row, Col, Modal, Form } from 'antd';

import Loading from './../../../components/LoadingIndicator';
import messages from './messages';
import genv from '../../../env';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as pageActions from './actions';

class ProfilePage extends React.Component {

  static PropTypes = {
    respPaymentTypes: React.PropTypes.instanceOf(PromiseState).isRequired,
    respStatusesList: React.PropTypes.instanceOf(PromiseState).isRequired,
    respOrder: React.PropTypes.instanceOf(PromiseState).isRequired
  };

  state = {
    data: {},
    changePass: false,
    formLayout: 'horizontal',
    hideAddress: false,
  };

  componentWillMount = () => {
    const {
      data,
    } = this.props;
    this.setState({
      data: data,
    });
  };

  componentWillReceiveProps = (nextProps) => {
    if(!nextProps.respOrder.pending && nextProps.respOrder.fulfilled) {
      nextProps.pageActions.dataUpdate({ data: nextProps.respOrder.value.data });
      this.setState({
        data: nextProps.respOrder.value.data,
      });
    }
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  isDelete = () => {
    Modal.confirm({
      title: 'Want to delete these items?',
      content: 'When clicked the OK button, this dialog will be closed after 1 second',
      onOk() {
        return new Promise((resolve, reject) => {
          setTimeout(Math.random() > 0.5 ? resolve : reject, 1000);
        }).catch(() => console.log('Oops errors!'));
      },
      onCancel() {},
    });
  };

  showHideAddress = () => {
    this.setState({
      hideAddress: !this.state.hideAddress
    })
  };

  Restore = (data) => {
    this.setState({
      data,
    });
  };

  render = () => {
    const {
      data,
      formLayout,
    } = this.state;
    const {
      respPaymentTypes,
      respStatusesList,
      respOrder,
    } = this.props;
    const { getFieldDecorator } = this.props.form;
    const allFetches = PromiseState.all([
      respPaymentTypes || {},
      respStatusesList || {},
      respOrder || {},
    ]);
    const [payments, statuses, order] = allFetches.value;
    return (allFetches.settled) ? (
      <Form layout={formLayout} onSubmit={this.handleSubmit}>
        <div  style={{ paddingTop: 20 }}/>
        <Row>
          <Col span={16} offset={4}>
            <Row>
              <Col span={4}>
                <span style={{ fontSize: 13, top: 7, position: 'relative' }}>
                  Name :
                </span>
              </Col>
              <Col span={20}>
                <Form.Item>
                  {getFieldDecorator('name', {
                    rules: [{ required: true, message: 'Please input your order name!' }],
                    initialValue: data.name
                  })(
                    <Input
                      placeholder="input placeholder"
                      onChange={({ target: { value }}) => (this.setState({
                          data: {
                            ...data,
                            name: value,
                          }
                        })
                      )}
                    />
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Col>
          <Col span={16} offset={4}>
            <Row>
              <Col span={4}>
                <span style={{ fontSize: 13, top: 7, position: 'relative' }}>
                  Status :
                </span>
              </Col>
              <Col span={20}>
                <Form.Item>
                  {getFieldDecorator('status', {
                    rules: [{ required: true, message: 'Please input your status!' }],
                  })(
                    <Select
                      key="select_status_order_page_form"
                      showSearch
                      style={{ width: '100%' }}
                      placeholder="Select a status"
                      optionFilterProp="children"
                      onChange={(value) => (this.setState({
                          data: {
                            ...data,
                            status: _.find(statuses.data, { number: value }),
                          }
                        })
                      )}
                      filterOption={(input, option) => option.props.value.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                      {statuses.data.map((item) => (
                        <Select.Option key={`selected_status_order_page_form_${item.number}`} value={`${item.number}`}>{item.name}</Select.Option>)
                      )}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Col>
          <Col span={16} offset={4}>
            <Row>
              <Col span={4}>
                <span style={{ fontSize: 13, top: 7, position: 'relative' }}>
                  Route :
                </span>
              </Col>
              <Col span={20}>
                <Form.Item>
                  {getFieldDecorator('route', {
                    rules: [{ required: true, message: 'Please input your route!' }],
                    initialValue: data.route
                  })(
                    <Input placeholder="input placeholder" onChange={({ target: { value }}) => this.changeRoute(value) }/>
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Col>
          <Col span={16} offset={4}>
            <Row>
              <Col span={4}>
                <span style={{ fontSize: 13, top: 4, position: 'relative' }}>
                  Address :
                </span>
              </Col>
              <Col span={20}>
                <Form.Item>
                  <Button onClick={() => (this.showHideAddress())}>
                    {(this.state.hideAddress)?(<span>Hide</span>):(<span>Show</span>)}
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Col>
          {(this.state.hideAddress)?(
            <div>
              <Col span={16} offset={4}>
                <Row>
                  <Col span={4} >
                    <span style={{ fontSize: 13, top: 7, position: 'relative', left: 20 }}>
                      Country :
                    </span>
                  </Col>
                  <Col span={20}>
                    <Form.Item>
                      {getFieldDecorator('country', {
                        rules: [{ required: true, message: 'Please input your country!' }],
                        initialValue: data.address.country
                      })(
                        <Input
                          placeholder="input placeholder"
                          onChange={({ target: { value }}) => this.setState((prevState) => ({
                              ...prevState,
                              data: {
                                ...prevState.data,
                                address: {
                                  ...prevState.data.address,
                                  country: value,
                                }
                              }
                            }))}
                        />
                      )}
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col span={16} offset={4}>
                <Row>
                  <Col span={4} >
                    <span style={{ fontSize: 13, top: 7, position: 'relative', left: 20 }}>
                      Street-1 :
                    </span>
                  </Col>
                  <Col span={20}>
                    <Form.Item>
                      {getFieldDecorator('street1', {
                        rules: [{ required: true, message: 'Please input your first street!' }],
                        initialValue: data.address.street1
                      })(
                        <Input
                          placeholder="input placeholder"
                          onChange={({ target: { value }}) => this.setState((prevState) => ({
                            ...prevState,
                            data: {
                              ...prevState.data,
                              address: {
                                ...prevState.data.address,
                                street1: value,
                              }
                            }
                          }))}
                        />
                      )}
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col span={16} offset={4}>
                <Row>
                  <Col span={4} >
                    <span style={{ fontSize: 13, top: 7, position: 'relative', left: 20 }}>
                      Street-2 :
                    </span>
                  </Col>
                  <Col span={20}>
                    <Form.Item>
                      {getFieldDecorator('street2', {
                        rules: [{ required: false, message: 'Please input your first street!' }],
                        initialValue: data.address.street2
                      })(
                        <Input
                          placeholder="input placeholder"
                          onChange={({ target: { value }}) => this.setState((prevState) => ({
                            ...prevState,
                            data: {
                              ...prevState.data,
                              address: {
                                ...prevState.data.address,
                                street2: value,
                              }
                            }
                          }))}
                        />
                      )}
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col span={16} offset={4}>
                <Row>
                  <Col span={4} >
                    <span style={{ fontSize: 13, top: 7, position: 'relative', left: 20 }}>
                      Postcode :
                    </span>
                  </Col>
                  <Col span={20}>
                    <Form.Item>
                      {getFieldDecorator('street2', {
                        rules: [{ required: true, message: 'Please input your postcode!' }],
                        initialValue: data.address.postcode
                      })(
                        <Input
                          placeholder="input placeholder"
                          onChange={({ target: { value }}) => this.setState((prevState) => ({
                            ...prevState,
                            data: {
                              ...prevState.data,
                              address: {
                                ...prevState.data.address,
                                postcode: value,
                              }
                            }
                          }))}
                        />
                      )}
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
            </div>
          ):(<div />)}
          <Col span={16} offset={4}>
            <Row>
              <Col span={4} >
                <span style={{ fontSize: 13, top: 7, position: 'relative' }}>
                  Payment type :
                </span>
              </Col>
              <Col span={20}>
                <Form.Item>
                  {getFieldDecorator('paymentType', {
                    rules: [{ required: true, message: 'Please select your payment type!' }],
                  })(
                    <Select
                      key="select_payment_type_page_form"
                      showSearch
                      style={{ width: '100%' }}
                      placeholder="Select a payment type"
                      optionFilterProp="children"
                      onChange={(value) => (this.setState({
                          data: {
                            ...data,
                            payment: _.find(payments.data, { number: value }),
                          }
                        })
                      )}
                      filterOption={(input, option) => option.props.value.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                      {payments.data.map((item) => (
                        <Select.Option key={`selected_payment_type_page_form_${item.number}`} value={`${item.number}`}>{item.name}</Select.Option>)
                      )}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Col>
          <Col span={16} offset={4}>
            <Row>
              <Col span={4} >
                <span style={{ fontSize: 13, position: 'relative' }}>
                  Date :
                </span>
              </Col>
              <Col span={20}>
                {(moment(data.createdAt)).format("dddd, MMMM Do YYYY, h:mm:ss a")}
              </Col>
            </Row>
          </Col>
          <Col span={16} offset={4}>
            <div style={{ padding: 10 }}/>
            <Row>
              <Col span={24} >
                <div style={{ marginRight: 10, display: 'inline-block' }}>
                  <Button type="primary" htmlType="submit">Save</Button>
                </div>
                {/*<div style={{ marginRight: 10, display: 'inline-block' }}>
                  <Button onClick={() => (this.Restore(order.data))} >Restore</Button>
                </div>*/}
                <div style={{ marginRight: 10, display: 'inline-block' }}>
                  <Button onClick={() => (this.isDelete())} type="danger">Delete</Button>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    ) : (<Loading />);
  }
}

const FormOrderRefetch = Form.create()(ProfilePage);

const {
  protocol,
  serverAddr,
  port,
} = genv.request;

const OrderRefetch = connectRefetch(props => ({
  respOrder: {
    url: `${protocol}${serverAddr}:${port}/user/order`,
    headers: {
      Authorization: props.authData['auth-token'],
    },
    method: 'GET'
  },
  respPaymentTypes: {
    url: `${protocol}${serverAddr}:${port}/user/payment-types`,
    headers: {
      Authorization: props.authData['auth-token'],
    },
    method: 'GET',
  },
  respStatusesList: {
    url: `${protocol}${serverAddr}:${port}/user/statuses-list`,
    headers: {
      Authorization: props.authData['auth-token'],
    },
    method: 'GET',
  }
}))(FormOrderRefetch);

export default connect((state) => ({
  data: state.toJS().order ? state.toJS().order.data : {},
  authData: state.toJS().authData ? state.toJS().authData.auth : {},
}), (dispatch) => ({
  pageActions: bindActionCreators(pageActions, dispatch)
}))(OrderRefetch);
