import { fromJS } from 'immutable';
import _ from 'lodash';

import {
  data_UPDATE,
  data_DELETE,
} from './constants';

const initialState = fromJS({
  data: [],
  delete: {}
});

const battlersReducer = (state = initialState, action) => {
  switch (action.type) {
    case data_UPDATE:
      return state
        .set('data', action.payload.data);
    default:
      return state;
  }
};

export default battlersReducer;
