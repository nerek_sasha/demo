import {
  data_UPDATE,
  data_DELETE,
  data_CREATE,
} from './constants';

export const dataUpdate = (payload = {}) => ({
  type: data_UPDATE,
  payload,
});

export const dataCreate = (payload = {}) => ({
  type: data_CREATE,
  payload,
});

export const dataDelete = (payload = {}) => ({
  type: data_DELETE,
  payload,
});
