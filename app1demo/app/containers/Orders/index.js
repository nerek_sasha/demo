import React from 'react';
import { connect as connectRefetch } from 'react-refetch'
import { Link } from 'react-router';
import { Table, Icon, Button, Checkbox, Input, Dropdown, Menu, Row, Col } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';
import * as pageActions from './actions';
import AddOrder from './AddOrder';
import genv from '../../env';

const RowActions = styled(Row)`
  padding: 10px 0;
`;

const sortAlphabet = (a, b) => {
  if(a < b) return -1;
  if(a > b) return 1;
  return 0;
};

export class Orders extends React.PureComponent {

  static propTypes = {
    pageActions: React.PropTypes.shape().isRequired,
  };

  state = {
    data: [],
    loading: true,
    isSync: false,
    syncIsOK: false,
    visibleModalAddOrder: false,
  };

  componentWillMount = () => {
    this.props.reqOrders();
    const {
      data,
    } = this.props;
    if (data.length > 0) {
      console.log(data, 'loading: false');
      this.setState({
        data: data,
        loading: false,
      });
    } else {
      console.log('loading: ', true);
      this.setState({
        data: [],
        loading: true,
      });
    }
  };

  componentWillReceiveProps = (nextProps) => {
    if(nextProps.respOrders && !nextProps.respOrders.pending && nextProps.respOrders.fulfilled){
      this.props.pageActions.dataUpdate({ data: nextProps.respOrders.value.data });
      this.setState({
        isSync: true,
        syncIsOK: true,
        loading: false,
        data: nextProps.respOrders.value.data,
      });
    } else if ( nextProps.respOrders && nextProps.respOrders.rejected ) {
      this.setState({
        isSync: true,
      });
    }
  };

  showAddOrder = () => {
    if( !this.state.visibleModalAddOrder ) {
      this.setState({
        visibleModalAddOrder: true
      });
    }
  };

  render = () => {
    const {
      data,
      loading,
    } = this.state;
    console.log(loading);
    const columns = [{
      title: 'Name',
      dataIndex: 'name',
      render: (name, row) => {
        return (<Link to={`/order/${row._id}`}>{name}</Link>);
      },
      sorter: (a, b) => {
        const A = a.name.last.toLowerCase(),
          B = b.name.last.toLowerCase();
        return sortAlphabet(A, B);
      },
    }, {
      title: 'Time create',
      dataIndex: 'createdAt',
      render: (createdAt) => {
        return (<span>{createdAt}</span>);
      },
    }];
    return (
    <div style={{ paddingTop: 10 }}>
      <div>
        <RowActions >
          <Col span={8} >
            <Button type="primary" onClick={() => this.showAddOrder()}>
              <Icon type="plus" /> Add Order
            </Button>
          </Col>
        </RowActions>
      </div>
      <Table
        columns={columns}
        rowKey={record => record._id}
        dataSource={data}
        loading={loading}
      />
      <AddOrder
        visible={this.state.visibleModalAddOrder}
        OnClose={() => {
          this.setState({
            visibleModalAddOrder: false
          });
        }}
      />
    </div>
    );
  }
}

const OrdersRefetch = connectRefetch(props => ({
  reqOrders: () => {
    const {
      protocol,
      serverAddr,
      port,
    } = genv.request;
    return ({
      respOrders: {
        url: `${protocol}${serverAddr}:${port}/user/orders`,
        method: 'GET',
        force: true,
        refreshing: true,
      },
    });
  },
}))(Orders);

export default connect((state) => ({
  data: state.toJS().orders ? state.toJS().orders.data : []
}), (dispatch) => ({
  pageActions: bindActionCreators(pageActions, dispatch)
}))(OrdersRefetch);
