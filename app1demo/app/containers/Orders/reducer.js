import { fromJS } from 'immutable';
import _ from 'lodash';

import {
  data_UPDATE,
  data_DELETE,
  data_CREATE,
} from './constants';

const initialState = fromJS({
  data: [],
  delete: {},
  create: {},
});

const ordersReducer = (state = initialState, action) => {
  switch (action.type) {
    case data_UPDATE:
      return state
        .set('data', action.payload.data);
    case data_DELETE:
      let tDataDelete = state.get('data');
      tDataDelete = _.remove(tDataDelete, item => (item._id !== action.payload.delete));
      return state
        .set('data', tDataDelete);
    case data_CREATE:
      const tDataCreate = state.get('data');
      tDataCreate.unshift(action.payload.create);
      return state
        .set('data', tDataCreate);
    default:
      return state;
  }
};

export default ordersReducer;
