import { fromJS } from 'immutable';
import { combineReducers } from 'redux-immutable';
import { LOCATION_CHANGE } from 'react-router-redux';

import globalReducer from 'containers/App/reducer';
import languageProviderReducer from 'containers/LanguageProvider/reducer';
import dataBattlersReducer from 'containers/Battlers/reducer';
import dataClientsReducer from 'containers/Clients/reducer';
import AuthAdminReducer from 'components/Auth/reducer';

const routeInitialState = fromJS({
  locationBeforeTransitions: null,
});

function routeReducer(state = routeInitialState, action) {
  switch (action.type) {
    case LOCATION_CHANGE:
      return state.merge({
        locationBeforeTransitions: action.payload,
      });
    default:
      return state;
  }
}

export default function createReducer(asyncReducers) {
  return combineReducers({
    route: routeReducer,
    global: globalReducer,
    language: languageProviderReducer,
    dataBattlers: dataBattlersReducer,
    dataClients: dataClientsReducer,
    authData: AuthAdminReducer,
    ...asyncReducers,
  });
}
