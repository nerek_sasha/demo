import React from 'react';
import Helmet from 'react-helmet';
import { IntlProvider } from 'react-intl';
import { connect } from 'cerebral/react';
import { state } from 'cerebral/tags';

import { messages } from 'app/i18n';

// @decorator({
//   buttonClicked: signal`buttonClicked`,
//   current_page: state`current_page`,
//   locale: state`locale`,
//   title: state`title`,
// })
// export default class Container extends React.Component {
//   static propTypes = {
//     buttonClicked: React.PropTypes.func.isRequired,
//     current_page: React.PropTypes.string.isRequired,
//     locale: React.PropTypes.string.isRequired,
//     title: React.PropTypes.string.isRequired,
//   };
//   render = () => {
//     const {
//       buttonClicked,
//       current_page,
//       locale,
//       title,
//     } = this.props;
//     return (
//       <IntlProvider locale={locale} messages={messages[locale]}>
//         <div>
//           <Helmet
//             htmlAttributes={{ lang: locale, amp: 'undefined' }}
//             title={title}
//           />
//           <h1>{title} {current_page}</h1>
//           <button onClick={() => buttonClicked()}>test</button>
//         </div>
//       </IntlProvider>
//     );
//   };
// }

const Container = (
  {
    current_page,
    locale,
    title,
  }
) => (
  <IntlProvider locale={locale} messages={messages[locale]}>
    <div>
      <Helmet
        htmlAttributes={{ lang: locale, amp: 'undefined' }}
        title={title}
      />
      <h1>{title} {current_page}</h1>
    </div>
  </IntlProvider>
);
Container.propTypes = {
  current_page: React.PropTypes.string.isRequired,
  locale: React.PropTypes.string.isRequired,
  title: React.PropTypes.string.isRequired,
};
export default connect({
  current_page: state`current_page`,
  locale: state`locale`,
  title: state`title`,
}, Container);
