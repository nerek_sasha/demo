import { Controller } from 'cerebral';
import Devtools from 'cerebral/devtools';
import Router from 'cerebral-router';

import {
  controller as index,
} from 'modules/index';
import {
  controller as not_found,
} from 'modules/not_found';

const controller = Controller({
  devtools: process.env.NODE_ENV === 'production' ? null : Devtools(),
  router: Router({
    routes: {
      '/': 'index.routed',
      '/not_found/': 'not_found.routed',
    },
  }),
  modules: {
    index,
    not_found,
  },
  state: {
    current_page: 'index',
    locale: 'ru',
    title: 'Hello world',
  },
});

export default controller;
