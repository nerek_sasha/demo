/**
 * Application config
 **/
const config = {
  appLocales: ['en', 'ru'],
  appVersion: 'v0.1.0',
  sentryDsn: ''
};

export default config;
