import { addLocaleData } from 'react-intl';
import enLocaleData from 'react-intl/locale-data/en';
import ruLocaleData from 'react-intl/locale-data/ru';
import enTranslationMessages from 'app/translations/en';
import ruTranslationMessages from 'app/translations/ru';

addLocaleData(enLocaleData);
addLocaleData(ruLocaleData);

export const formatTranslationMessages = (messages) => {
  const formattedMessages = {};
  messages.forEach((message) => {
    formattedMessages[message.id] = message.message || message.defaultMessage;
  });
  return formattedMessages;
};

export const messages = {
  en: formatTranslationMessages(enTranslationMessages),
  ru: formatTranslationMessages(ruTranslationMessages)
};
