import { defineMessages } from 'react-intl';

const name = 'app';

const messages = defineMessages({
  title: {
    id: `${name}.title`,
    defaultMessage: 'Hello world !!',
  },
});

export default messages;
