import React from 'react';
import Helmet from 'react-helmet';

const Container = (
  {
    content,
    state,
    styles,
  }
) => {
  const helmet = Helmet.rewind();
  const attributes = helmet.htmlAttributes.toComponent();
  return (
    <html lang="ru" {...attributes}>
      <head>
        {helmet.title.toComponent()}
        {helmet.meta.toComponent()}
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
        />
        <meta httpEquiv="content-type" content="text/html; charset=utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge" />
        <meta name="robots" content="all" />
        <link rel="stylesheet" href="/assets/app.css" />
        <style dangerouslySetInnerHTML={{ __html: styles }} />
      </head>
      <body>
        <div id="application" dangerouslySetInnerHTML={{ __html: content }} />
        <script type="text/javascript" dangerouslySetInnerHTML={{ __html: state }} />
        <script type="text/javascript" src="/assets/vendor.js" />
        <script type="text/javascript" src="/assets/app.js" />
      </body>
    </html>
  );
};
Container.propTypes = {
  content: React.PropTypes.string.isRequired,
  state: React.PropTypes.string.isRequired,
  styles: React.PropTypes.string.isRequired
};
export default Container;
