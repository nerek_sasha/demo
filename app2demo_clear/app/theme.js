import {
  blue500,
  cyan700,
  darkBlack,
  fullBlack,
  fullWhite,
  grey100,
  grey300,
  grey400,
  grey500,
  pinkA200,
  white
} from 'material-ui/styles/colors';
import { fade } from 'material-ui/utils/colorManipulator';
import spacing from 'material-ui/styles/spacing';

export const muiTheme = {
  spacing,
  fontFamily: 'Roboto, sans-serif',
  palette: {
    primary1Color: blue500,
    primary2Color: cyan700,
    primary3Color: grey400,
    accent1Color: '#115082',
    accent2Color: grey100,
    accent3Color: '#212121',
    textColor: '#212121',
    secondaryTextColor: fade(darkBlack, 0.54),
    alternateTextColor: '#efefef',
    canvasColor: white,
    borderColor: '#f5f5f5',
    disabledColor: fade(darkBlack, 0.3),
    pickerHeaderColor: fade(fullWhite, 0.12),
    clockCircleColor: fade(darkBlack, 0.12),
    shadowColor: darkBlack
  },
  tableRow: {
    selectedColor: '#d8edff',
    stripeColor: '#f9f9f9'
  }
};

export const theme = {
  flex: {
    gutters: '1.6rem'
  },
  gutters: '1.6rem',
};
