import { Container } from 'cerebral/react';
import IsomorphicRelay from 'isomorphic-relay';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import MobxDevTools from 'mobx-react-devtools';
import Raven from 'raven-js';
import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import Relay from 'react-relay';
import { RelayNetworkLayer, urlMiddleware } from 'react-relay-network-layer';
import { ThemeProvider } from 'styled-components';

import Client, { controller } from 'app/client';
import config from 'app/config';
import 'app/init';
import { theme, muiTheme } from 'app/theme';

const data = [];
const initialState = {};
// const data = window.__DATA__;
// const initialState = window.__STATE__;
const appElement = document.getElementById('application');
let appConfig = config;
let appController = controller;
let appMuiTheme = getMuiTheme(muiTheme);
let appTheme = theme;

if (appConfig.sentryDsn) {
  Raven.config(appConfig.sentryDsn).install();
}

const relay = new Relay.Environment();
relay.injectNetworkLayer(new RelayNetworkLayer([
  urlMiddleware({ url: '/graphql' })
]));
IsomorphicRelay.injectPreparedData(relay, data);

const appRender = () => {
  ReactDOM.render(
    <AppContainer key={Math.random()}>
      <div>
        {process.env.NODE_ENV !== 'production' ? (
          <MobxDevTools
            position={{
              right: 48,
              top: 0
            }}
          />
        ) : null}
        <MuiThemeProvider muiTheme={appMuiTheme}>
          <ThemeProvider theme={appTheme}>
            <Container controller={appController}>
              <Client />
            </Container>
          </ThemeProvider>
        </MuiThemeProvider>
      </div>
    </AppContainer>,
    appElement
  );
};

const render = () => {
  try {
    appRender();
  } catch (error) {
    throw error;
  }
};

if (!window.Intl) {
  (new Promise((resolve) => {
    resolve(System.import('intl'));
  }))
    .then(() => Promise.all([
      System.import('intl/locale-data/jsonp/en.js'),
      System.import('intl/locale-data/jsonp/ru.js')
    ]))
    .then(() => {
      render();
    })
    .catch(() => { // error
      render(); // throw error;
    });
} else {
  render();
}

if (process.env.NODE_ENV !== 'production') {
  if (module.hot) {
    module.hot.accept();
    module.hot.accept([
      'app/config'
    ], () => {
      appConfig = require('app/config').default;
      render();
    });
    // module.hot.accept([
    //   'app/controller'
    // ], () => {
    //   appController = require('controller.js').default;
    //   render();
    // });
    module.hot.accept([
      'app/theme'
    ], () => {
      const nextThemes = require('app/theme');
      appMuiTheme = getMuiTheme(nextThemes.muiTheme);
      appTheme = nextThemes.theme;
      render();
    });
  }
}
