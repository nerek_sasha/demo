import gulp from 'gulp';
import requireDir from 'require-dir';

requireDir('./internals/tasks/', { recurse: true });

gulp.task('default', gulp.parallel(
  'intl:watch',
  'start:development'
));
