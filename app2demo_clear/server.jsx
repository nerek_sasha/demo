import React from 'react';
import { renderToString, renderToStaticMarkup } from 'react-dom/server';
import Helmet from 'react-helmet';
import serialize from 'serialize-javascript';
import { Container, StateContainer } from 'cerebral/react';

import Client from 'app/client';
import Server, { controller } from 'app/server';

export const serverRenderer = () => (req, res) => {
  const appController = controller;
  try {
    const content = renderToString(
      <Container controller={appController}>
        <Client />
      </Container>
    );
    const doctype = '<!DOCTYPE html>\n';
    const payload = `window.__DATA__ = ${serialize({})};`;
    const state = `window.__STATE__ = ${serialize({})};`;
    const styles = ''; // const styles = StyleSheet.rules().map(rule => rule.cssText).join('\n');
    const helmet = Helmet.rewind();
    const markup = (
      <Server
        {...{
          content,
          helmet,
          payload,
          state,
          styles
        }}
      />
    );
    const html = renderToStaticMarkup(markup);
    res.setHeader('Content-Type', 'text/html');
    return res.status(200).send(doctype + html);
  } catch (error) { return res.status(500).send(`${error}`); }
};
export default serverRenderer;

// res.status(200).send(`<!DOCTYPE html>
// <html lang="ru">
// <head>
//     <link rel="stylesheet" href="/assets/app.css" />
// </head>
// <body>
//     <div id="application"></div>
//     <script type="text/javascript" src="/assets/vendor.js"></script>
//     <script type="text/javascript" src="/assets/app.js"></script>
// </body>
// </html>`)
